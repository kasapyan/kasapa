<?php

$type = 'css';
$base = realpath('.');
$elements = array();

if(isset($_GET['ie']) && $_GET['ie'] == '6'){
    $elements = array('ie6hacks.css');
} elseif(isset($_GET['ie']) && $_GET['ie'] == '7'){
    $elements = array('ie7hacks.css');
} elseif(isset($_GET['print'])){
    $elements = array('print.css');
} else {
    $elements = array(
        'unified_alerts.css'=>TRUE,
        'custom.css',
    );
}

include($_SERVER['DOCUMENT_ROOT'] . '/facilities/files/css/combinator.php');

?>
